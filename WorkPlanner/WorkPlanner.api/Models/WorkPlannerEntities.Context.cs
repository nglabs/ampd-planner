﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WorkPlanner.api.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class WorkPlannerEntities1 : DbContext
    {
        public WorkPlannerEntities1()
            : base("name=WorkPlannerEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Collection> Collections { get; set; }
        public virtual DbSet<CollectionType> CollectionTypes { get; set; }
        public virtual DbSet<Milestone> Milestones { get; set; }
        public virtual DbSet<Outage> Outages { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<Work> Works { get; set; }
        public virtual DbSet<WorkType> WorkTypes { get; set; }
        public virtual DbSet<Zone> Zones { get; set; }
    }
}
