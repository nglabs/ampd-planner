﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkPlanner.api.Models
{
    public class CollectionDTO
    {
        public int CollectionID { get; set; }
        public int? CollectionTypeID { get; set; }
        public string Name { get; set; }
        public CollectionTypeDTO CollectionType { get; set; }
        public List<OutageDTO> Outages { get; set; }
    }

    public class CollectionTypeDTO
    {
        public int CollectionTypeID { get; set; }
        public string Name { get; set; }
    }

    public class OutageDTO
    {
        public int OutageID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? TargetDate { get; set; }
        public int? Duration { get; set; }
        public int? CollectionID { get; set; }
        public List<WorkDTO> Works { get; set; }
    }

    public class WorkDTO
    {
        public int WorkID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string WorkOrder { get; set; }
        public DateTime? TargetDate { get; set; }
        public int? Duration { get; set; }
        public decimal? Cost { get; set; }
        public int? EngineerManDaysRequired { get; set; }
        public int? SpecialistSkillsRequired { get; set; }
        public byte? PerformanceStatutoryObligation { get; set; }
        public byte? PerformanceNOM { get; set; }
        public byte? PerformanceCustomerConnection { get; set; }
        public int? RegionID { get; set; }
        public int? ZoneID { get; set; }
        public int? TeamID { get; set; }
        public int? WorkTypeID { get; set; }
        public int? OutageID { get; set; }
        public int? FigurativeRisk { get; set; }
        public DateTime? CommissionDate { get; set; }

        public RegionDTO Region { get; set; }
        public TeamDTO Team { get; set; }
        public WorkTypeDTO WorkType { get; set; }
        public ZoneDTO Zone { get; set; }
    }

    public class RegionDTO
    {
        public int RegionID { get; set; }
        public string Name { get; set; }
    }

    public class TeamDTO
    {
        public int TeamID { get; set; }
        public string Name { get; set; }
    }

    public class WorkTypeDTO
    {
        public int WorkTypeID { get; set; }
        public string Name { get; set; }
    }

    public class ZoneDTO
    {
        public int ZoneID { get; set; }
        public string Name { get; set; }
        public string Shortcode { get; set; }
    }
}