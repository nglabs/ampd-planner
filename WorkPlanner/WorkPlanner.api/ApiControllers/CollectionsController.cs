﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WorkPlanner.api.Models;

namespace WorkPlanner.api.ApiControllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CollectionsController : ApiController
    {
        private WorkPlannerEntities1 db = new WorkPlannerEntities1();

        // GET: api/Collections
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IQueryable<CollectionDTO> GetCollections()
        {

            return db.Collections.Select(c => new CollectionDTO()
            {
                CollectionID = c.CollectionID,
                Name = c.Name,
                CollectionTypeID = c.CollectionTypeID,
                CollectionType = db.CollectionTypes.Select(ct => new CollectionTypeDTO()
                {
                    CollectionTypeID = ct.CollectionTypeID,
                    Name = ct.Name
                }).FirstOrDefault(ct => ct.CollectionTypeID == c.CollectionTypeID),
                Outages = db.Outages.Select(o => new OutageDTO()
                {
                    OutageID = o.OutageID,
                    CollectionID = o.CollectionID,
                    Name = o.Name,
                    Description = o.Description,
                    TargetDate = o.TargetDate,
                    Duration = o.Duration,
                    Works = db.Works.Select(w => new WorkDTO()
                    {
                        WorkID = w.WorkID,
                        OutageID = w.OutageID,
                        Name = w.Name,
                        Description = w.Description,
                        WorkOrder = w.WorkOrder,
                        TargetDate = w.TargetDate,
                        Duration = w.Duration,
                        Cost = w.Cost,
                        EngineerManDaysRequired = w.EngineerManDaysRequired,
                        SpecialistSkillsRequired = w.SpecialistSkillsRequired,
                        PerformanceStatutoryObligation = w.PerformanceStatutoryObligation,
                        PerformanceNOM = w.PerformanceNOM,
                        PerformanceCustomerConnection = w.PerformanceCustomerConnection,
                        FigurativeRisk = w.FigurativeRisk,
                        CommissionDate = w.CommissionDate,
                        RegionID = w.RegionID,
                        ZoneID = w.ZoneID,
                        TeamID = w.TeamID,
                        WorkTypeID = w.WorkTypeID,
                        Region = db.Regions.Select(r => new RegionDTO() {
                            RegionID = r.RegionID,
                            Name = r.Name
                        }).FirstOrDefault(r => r.RegionID == w.RegionID),
                        Zone = db.Zones.Select(z => new ZoneDTO() {
                            ZoneID = z.ZoneID,
                            Name = z.Name,
                            Shortcode = z.Shortcode
                        }).FirstOrDefault(z => z.ZoneID == w.ZoneID),
                        Team = db.Teams.Select(t => new TeamDTO() {
                            TeamID = t.TeamID,
                            Name = t.Name
                        }).FirstOrDefault(t => t.TeamID == w.TeamID),
                        WorkType = db.WorkTypes.Select(wt => new WorkTypeDTO() {
                            WorkTypeID = wt.WorkTypeID,
                            Name = wt.Name
                        }).FirstOrDefault(wt => wt.WorkTypeID == w.WorkTypeID)
                    }).Where(w => w.OutageID == o.OutageID).ToList()
                }).Where(o => o.CollectionID == c.CollectionID).ToList()
            });
        }

        // GET: api/Collections/5
        [ResponseType(typeof(Collection))]
        public async Task<IHttpActionResult> GetCollection(int id)
        {
            Collection collection = await db.Collections.FindAsync(id);
            if (collection == null)
            {
                return NotFound();
            }

            return Ok(collection);
        }

        // PUT: api/Collections/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCollection(int id, Collection collection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != collection.CollectionID)
            {
                return BadRequest();
            }

            db.Entry(collection).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CollectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Collections
        [ResponseType(typeof(Collection))]
        public async Task<IHttpActionResult> PostCollection(Collection collection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Collections.Add(collection);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = collection.CollectionID }, collection);
        }

        // DELETE: api/Collections/5
        [ResponseType(typeof(Collection))]
        public async Task<IHttpActionResult> DeleteCollection(int id)
        {
            Collection collection = await db.Collections.FindAsync(id);
            if (collection == null)
            {
                return NotFound();
            }

            db.Collections.Remove(collection);
            await db.SaveChangesAsync();

            return Ok(collection);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CollectionExists(int id)
        {
            return db.Collections.Count(e => e.CollectionID == id) > 0;
        }
    }
}