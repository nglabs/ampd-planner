﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkPlanner.api.Models;

namespace WorkPlanner.api.Controllers
{
    public class OutagesController : Controller
    {
        private WorkPlannerEntities1 db = new WorkPlannerEntities1();

        // GET: Outages
        public async Task<ActionResult> Index()
        {
            var outages = db.Outages.Include(o => o.Collection);
            return View(await outages.ToListAsync());
        }

        // GET: Outages/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Outage outage = await db.Outages.FindAsync(id);
            if (outage == null)
            {
                return HttpNotFound();
            }
            return View(outage);
        }

        // GET: Outages/Create
        public ActionResult Create()
        {
            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name");
            return View();
        }

        // POST: Outages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OutageID,Name,Description,TargetDate,Duration,CollectionID")] Outage outage)
        {
            if (ModelState.IsValid)
            {
                db.Outages.Add(outage);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name", outage.CollectionID);
            return View(outage);
        }

        // GET: Outages/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Outage outage = await db.Outages.FindAsync(id);
            if (outage == null)
            {
                return HttpNotFound();
            }
            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name", outage.CollectionID);
            return View(outage);
        }

        // POST: Outages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OutageID,Name,Description,TargetDate,Duration,CollectionID")] Outage outage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(outage).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name", outage.CollectionID);
            return View(outage);
        }

        // GET: Outages/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Outage outage = await db.Outages.FindAsync(id);
            if (outage == null)
            {
                return HttpNotFound();
            }
            return View(outage);
        }

        // POST: Outages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Outage outage = await db.Outages.FindAsync(id);
            db.Outages.Remove(outage);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
