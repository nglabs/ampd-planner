﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkPlanner.api.Models;

namespace WorkPlanner.api.Controllers
{
    public class MilestonesController : Controller
    {
        private WorkPlannerEntities1 db = new WorkPlannerEntities1();

        // GET: Milestones
        public async Task<ActionResult> Index()
        {
            var milestones = db.Milestones.Include(m => m.Collection);
            return View(await milestones.ToListAsync());
        }

        // GET: Milestones/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Milestone milestone = await db.Milestones.FindAsync(id);
            if (milestone == null)
            {
                return HttpNotFound();
            }
            return View(milestone);
        }

        // GET: Milestones/Create
        public ActionResult Create()
        {
            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name");
            return View();
        }

        // POST: Milestones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MilestoneID,Name,Description,TargetDate,CollectionID")] Milestone milestone)
        {
            if (ModelState.IsValid)
            {
                db.Milestones.Add(milestone);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name", milestone.CollectionID);
            return View(milestone);
        }

        // GET: Milestones/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Milestone milestone = await db.Milestones.FindAsync(id);
            if (milestone == null)
            {
                return HttpNotFound();
            }
            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name", milestone.CollectionID);
            return View(milestone);
        }

        // POST: Milestones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MilestoneID,Name,Description,TargetDate,CollectionID")] Milestone milestone)
        {
            if (ModelState.IsValid)
            {
                db.Entry(milestone).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CollectionID = new SelectList(db.Collections, "CollectionID", "Name", milestone.CollectionID);
            return View(milestone);
        }

        // GET: Milestones/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Milestone milestone = await db.Milestones.FindAsync(id);
            if (milestone == null)
            {
                return HttpNotFound();
            }
            return View(milestone);
        }

        // POST: Milestones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Milestone milestone = await db.Milestones.FindAsync(id);
            db.Milestones.Remove(milestone);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
