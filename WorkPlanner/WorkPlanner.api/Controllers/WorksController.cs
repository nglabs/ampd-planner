﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkPlanner.api.Models;

namespace WorkPlanner.api.Controllers
{
    public class WorksController : Controller
    {
        private WorkPlannerEntities1 db = new WorkPlannerEntities1();

        // GET: Works
        public async Task<ActionResult> Index()
        {
            var works = db.Works.Include(w => w.Region).Include(w => w.Team).Include(w => w.WorkType).Include(w => w.Zone).Include(w => w.Outage);
            return View(await works.ToListAsync());
        }

        // GET: Works/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work work = await db.Works.FindAsync(id);
            if (work == null)
            {
                return HttpNotFound();
            }
            return View(work);
        }

        // GET: Works/Create
        public ActionResult Create()
        {
            ViewBag.OutageID = new SelectList(db.Outages, "OutageID", "Name");
            ViewBag.RegionID = new SelectList(db.Regions, "RegionID", "Name");
            ViewBag.TeamID = new SelectList(db.Teams, "TeamID", "Name");
            ViewBag.WorkTypeID = new SelectList(db.WorkTypes, "WorkTypeID", "Name");
            ViewBag.ZoneID = new SelectList(db.Zones, "ZoneID", "Name");
            return View();
        }

        // POST: Works/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "WorkID,Name,Description,WorkOrder,TargetDate,Duration,Cost,EngineerManDaysRequired,SpecialistSkillsRequired,PerformanceStatutoryObligation,PerformanceNOM,PerformanceCustomerConnection,RegionID,ZoneID,TeamID,WorkTypeID,OutageID,FigurativeRisk,CommissionDate")] Work work)
        {
            if (ModelState.IsValid)
            {
                db.Works.Add(work);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.OutageID = new SelectList(db.Outages, "OutageID", "Name", work.OutageID);
            ViewBag.RegionID = new SelectList(db.Regions, "RegionID", "Name", work.RegionID);
            ViewBag.TeamID = new SelectList(db.Teams, "TeamID", "Name", work.TeamID);
            ViewBag.WorkTypeID = new SelectList(db.WorkTypes, "WorkTypeID", "Name", work.WorkTypeID);
            ViewBag.ZoneID = new SelectList(db.Zones, "ZoneID", "Name", work.ZoneID);
            return View(work);
        }

        // GET: Works/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work work = await db.Works.FindAsync(id);
            if (work == null)
            {
                return HttpNotFound();
            }
            ViewBag.OutageID = new SelectList(db.Outages, "OutageID", "Name", work.OutageID);
            ViewBag.RegionID = new SelectList(db.Regions, "RegionID", "Name", work.RegionID);
            ViewBag.TeamID = new SelectList(db.Teams, "TeamID", "Name", work.TeamID);
            ViewBag.WorkTypeID = new SelectList(db.WorkTypes, "WorkTypeID", "Name", work.WorkTypeID);
            ViewBag.ZoneID = new SelectList(db.Zones, "ZoneID", "Name", work.ZoneID);
            return View(work);
        }

        // POST: Works/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "WorkID,Name,Description,WorkOrder,TargetDate,Duration,Cost,EngineerManDaysRequired,SpecialistSkillsRequired,PerformanceStatutoryObligation,PerformanceNOM,PerformanceCustomerConnection,RegionID,ZoneID,TeamID,WorkTypeID,OutageID,FigurativeRisk,CommissionDate")] Work work)
        {
            if (ModelState.IsValid)
            {
                db.Entry(work).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.OutageID = new SelectList(db.Outages, "OutageID", "Name", work.OutageID);
            ViewBag.RegionID = new SelectList(db.Regions, "RegionID", "Name", work.RegionID);
            ViewBag.TeamID = new SelectList(db.Teams, "TeamID", "Name", work.TeamID);
            ViewBag.WorkTypeID = new SelectList(db.WorkTypes, "WorkTypeID", "Name", work.WorkTypeID);
            ViewBag.ZoneID = new SelectList(db.Zones, "ZoneID", "Name", work.ZoneID);
            return View(work);
        }

        // GET: Works/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work work = await db.Works.FindAsync(id);
            if (work == null)
            {
                return HttpNotFound();
            }
            return View(work);
        }

        // POST: Works/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Work work = await db.Works.FindAsync(id);
            db.Works.Remove(work);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
