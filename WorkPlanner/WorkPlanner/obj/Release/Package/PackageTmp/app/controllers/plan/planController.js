﻿/// <reference path="C:\Dev\WorkPlanner\WorkPlanner\Scripts/jquery.canvasjs.min.js" />

(function () {

    var PlanController = function ($scope, $state, $location, $localStorage, $timeout, Settings) {

        $scope.settings = $localStorage.settings || Settings;

        if (Object.keys($scope.settings).length <= 0) {
            console.log('Redirecting, not set up...');
            $location.path('/');
        }
        else {
            $scope.state = $state.current;
            $scope.chart = "risk"; // Default chart to display
            $scope.tickWidth = 4;
            $scope.live = {};
            $scope.ganttData = $localStorage.ganttData || getGanttData($scope.settings.collections);
            $scope.durationInDays = moment($scope.settings.end, 'DD/MM/YYYY').diff(moment($scope.settings.start, 'DD/MM/YYYY'), 'days') + 1;

            function autosave() {
                console.log("Autosaving...");
                $localStorage.ganttData = $scope.ganttData;
                $localStorage.settings = $scope.settings;
            }

            setInterval(autosave(), 60000);

            // Generate the chart template
            $scope.chartTemplate = [];
            for (var i = 0; i < $scope.durationInDays; i++) {
                $scope.chartTemplate.push({
                    x: moment($scope.settings.start, 'DD/MM/YYYY').add(i, 'days').toDate(),
                    y: 0,
                });
            }

            $scope.calculateCharts = function () {
                autosave();
                // Instantiate all plot data containers
                var riskChartPlot = $.extend(true, [], $scope.chartTemplate);
                var costChartPlot = $.extend(true, [], $scope.chartTemplate);
                var engineerChartPlot = $.extend(true, [], $scope.chartTemplate);

                // Transform work data into chart plots
                var allWorkData = pluckAllWorkItems($scope.ganttData);
                var work;
                var xVal;
                var yVal;

                for (var w in allWorkData) {
                    work = allWorkData[w];

                    if (work.data.Planned == true) {
                        var xVal = new Date(work.tasks[0].from).valueOf();
                        var range = moment().range(work.tasks[0].from, work.tasks[0].to);
                        var commissionDate = new Date(work.data.CommissionDate).valueOf();

                        console.log(commissionDate)

                        // Risk

                        var coefficient = parseFloat(work.data.Risk);
                        var c = 0.1;
                        var m = 0.7;
                        var g = 0.5;
                        var variables = [[c, m, g]];
                        var check = 0;

                        angular.forEach(riskChartPlot, function (plot) {
                            if (commissionDate == null || moment(new Date(plot.x).valueOf()).diff(commissionDate) > 0) {
                                var worked = (new Date(plot.x).valueOf() == xVal && work.data.Planned == true);
                                var cTmp, mTmp, gTmp;

                                check = worked ? check + 1 : check;

                                cTmp = worked ? Math.max(0.1 * check, (variables[variables.length - 1][0] / 1.5)) : variables[variables.length - 1][0] + 0.03;
                                mTmp = worked ? 1.5 : variables[variables.length - 1][1] + cTmp;
                                gTmp = worked ? variables[variables.length - 1][2] + (variables.length / 10) : variables[variables.length - 1][2] + 0.3;

                                variables.push([cTmp, mTmp, gTmp]);

                                plot.y += (gTmp * (variables.length + coefficient) * mTmp) / (365000);
                            }
                        });

                        // Cost
                        yVal = work.data.Cost;

                        _.filter(costChartPlot, function (point) {
                            if (new Date(point.x).valueOf() == xVal) {
                                point.y += yVal;
                            }
                        });

                        // Engineer man days required
                        yVal = work.data.EngineerManDaysRequired / work.data.Duration;

                        _.filter(engineerChartPlot, function (point) {
                            if (range.contains(new Date(point.x))) {
                                point.y += yVal;
                            }
                        });
                    }
                };

                // Process charts

                costChartPlot = getCumulativePlot(costChartPlot);

                // Render charts

                var chartHeight = 220;
                var chartWidth = ($scope.durationInDays * $scope.tickWidth) + 8;

                var riskChart = new CanvasJS.Chart("riskChart", getStandardLineChartOptions(riskChartPlot, $scope.settings.riskLimit, '#26A69A', chartHeight, chartWidth, 'area'))
                riskChart.render();

                var costChart = new CanvasJS.Chart("costChart", getStandardLineChartOptions(costChartPlot, $scope.settings.budget, '#03A9F4', chartHeight, chartWidth, 'stepArea'));
                costChart.render();

                var engineerChart = new CanvasJS.Chart("engineerChart", getStandardLineChartOptions(engineerChartPlot, $scope.settings.engineers, '#673AB7', chartHeight, chartWidth, 'stepArea'));
                engineerChart.render();
            }

            $scope.calculateRiskChart = function (work) {

                // Instatiate all plot data containers
                var individualRiskChartPlot = $.extend(true, [], $scope.chartTemplate);

                var xVal = new Date(work.tasks[0].from).valueOf();
                var commissionDate = new Date(work.data.CommissionDate).valueOf();

                // Risk

                var coefficient = parseFloat(work.data.Risk);
                var c = 0.1;
                var m = 0.7;
                var g = 0.5;
                var variables = [[c, m, g]];
                var check = 0;

                angular.forEach(individualRiskChartPlot, function (plot) {
                    if (commissionDate == null || moment(new Date(plot.x).valueOf()).diff(commissionDate) > 0) {
                        var worked = (new Date(plot.x).valueOf() == xVal && work.data.Planned == true);
                        var cTmp, mTmp, gTmp;

                        check = worked ? check + 1 : check;

                        cTmp = worked ? Math.max(0.1 * check, (variables[variables.length - 1][0] / 1.5)) : variables[variables.length - 1][0] + 0.03;
                        mTmp = worked ? 1.5 : variables[variables.length - 1][1] + cTmp;
                        gTmp = worked ? variables[variables.length - 1][2] + (variables.length / 10) : variables[variables.length - 1][2] + 0.3;

                        variables.push([cTmp, mTmp, gTmp]);

                        plot.y = (gTmp * (variables.length + coefficient) * mTmp) / (365000);
                    }
                });

                var individualRiskChart = new CanvasJS.Chart("individualRiskChart", getRiskLineChartOptions(individualRiskChartPlot, commissionDate, $scope.settings.riskLimit, 300, null));

                individualRiskChart.render();
            };

            $scope.planWork = function (id, isPlanned) {
                for (var i in $scope.ganttData) {
                    if ($scope.ganttData[i].id == id) {

                        $scope.ganttData[i].data.Planned = isPlanned;
                        $scope.ganttData[i].tasks[0].color = getTaskColor($scope.ganttData[i].data.WorkType, isPlanned);
                        $scope.calculateCharts();
                        $scope.calculateRiskChart($scope.ganttData[i]);
                    }
                }
            }

            // Gantt Options

            $scope.headerFormats = {
                month: 'MMM YY'
            }

            $scope.ganttOptions = {
                mode: 'custom',
                viewScale: 'day',
                columnMagnet: '1 day',
                headers: ['year', 'month'],
                fromDate: moment($scope.settings.start, 'DD/MM/YYYY').format('MM/DD/YYYY'),
                toDate: moment($scope.settings.end, 'DD/MM/YYYY').format('MM/DD/YYYY'),
                currentDateValue: new Date(),
                data: $scope.ganttData,
                daily: true,
                width: true,
                columnWidth: $scope.tickWidth,
                allowSideResizing: false,
                api: function (api) {
                    $scope.api = api;

                    api.core.on.ready($scope, function () {
                        api.directives.on.new($scope, function (directiveName, directiveScope, element) {
                            switch (directiveName) {
                                case 'ganttRowLabel':
                                    element.bind('dblclick', function () {

                                        $scope.live.row = directiveScope.row.model;
                                        $scope.$digest();
                                        if (directiveScope.row.model.classes == 'gantt-task-outage') {

                                            var groupRiskChartPlot = $.extend(true, [], $scope.chartTemplate);

                                            angular.forEach(directiveScope.row.model.children, function (taskId) {
                                                var workArray = _.where($scope.ganttData, { id: taskId });

                                                if (workArray.length > 0) {
                                                    var work = workArray[0];

                                                    if (work.data.Planned == true) {
                                                        var xVal = new Date(work.tasks[0].from).valueOf();
                                                        var commissionDate = new Date(work.data.CommissionDate).valueOf();

                                                        // Risk

                                                        var coefficient = parseFloat(work.data.Risk);
                                                        var c = 0.1;
                                                        var m = 0.7;
                                                        var g = 0.5;
                                                        var variables = [[c, m, g]];
                                                        var check = 0;

                                                        angular.forEach(groupRiskChartPlot, function (plot) {
                                                            if (commissionDate == null || moment(new Date(plot.x).valueOf()).diff(commissionDate) > 0) {
                                                                var worked = (new Date(plot.x).valueOf() == xVal && work.data.Planned == true);
                                                                var cTmp, mTmp, gTmp;

                                                                check = worked ? check + 1 : check;

                                                                cTmp = worked ? Math.max(0.1 * check, (variables[variables.length - 1][0] / 1.5)) : variables[variables.length - 1][0] + 0.03;
                                                                mTmp = worked ? 1.5 : variables[variables.length - 1][1] + cTmp;
                                                                gTmp = worked ? variables[variables.length - 1][2] + (variables.length / 10) : variables[variables.length - 1][2] + 0.3;

                                                                variables.push([cTmp, mTmp, gTmp]);

                                                                plot.y += (gTmp * (variables.length + coefficient) * mTmp) / (365000);
                                                            }

                                                            
                                                        });

                                                    }
                                                }
                                            });

                                            var individualRiskChart = new CanvasJS.Chart("groupRiskChart", getRiskLineChartOptions(groupRiskChartPlot, null, $scope.settings.riskLimit, 300, null));
                                            individualRiskChart.render();

                                            $('#GroupModal').modal('show');
                                        }

                                        

                                        $scope.live.row = directiveScope.row.model;
                                    });
                                    break;
                                case 'ganttTask':
                                    element.bind('dblclick', function (event) {
                                        event.stopPropagation();

                                        // Check if task or milestone
                                        if (!element.hasClass('gantt-task-milestone')) {
                                            $scope.live.task = directiveScope.task.model;
                                            $scope.live.row = directiveScope.row.model;

                                            $scope.$digest();

                                            $('#TaskModal').modal('show');

                                            // Generate risk profile
                                            $scope.calculateRiskChart($scope.live.row);
                                        }
                                    });
                                    break;
                                default:
                                    break;
                            }
                        });

                        api.tasks.on.change($scope, function (directiveScope) {
                            if (!directiveScope.$element.hasClass('gantt-task-milestone')) {
                                $scope.planWork(directiveScope.row.model.id, true);
                            }
                        });

                        $scope.calculateCharts();
                    });
                    
                    var scrolled = 0;

                    $('.scroll-left').on('click', function () {
                        if (scrolled <= 0) {
                            scrolled = 0;
                        } else {
                            scrolled = scrolled - 200;
                        }

                        $('.gantt-scrollable, .chart-container').stop().animate({
                            scrollLeft: scrolled
                        }, 200);
                    });

                    $('.scroll-right').on('click', function () {

                        var scrollableArea = $('.gantt-body').innerWidth() - $('.gantt-scrollable').innerWidth();

                        if (scrolled < scrollableArea) {
                            scrolled = scrolled + 200;
                        }

                        $('.gantt-scrollable, .chart-container').stop().animate({
                            scrollLeft: scrolled
                        }, 200);

                    });
                }
            }
        }
    };

    angular.module('workPlannerApp').controller('PlanController', PlanController);

}());

function getTaskColor(type, planned) {

    var color = '#CFD8DC';

    if (planned) {
        switch (type) {
            case 'Outage Scheme':
                color = '#2196F3';
                break;
            case 'Outage Maintenance':
                color = '#4CAF50';
                break;
            case 'Non-outage':
                color = '#FDD835';
                break;
            default:
                break;
        }
    }

    return color;
}

function getStandardLineChartOptions(data, threshold, color, height, width, type) {

    var options = {};

    if (height != null)
        options.height = height;

    if (width != null)
        options.width = width;
    
    options.axisX = {
        margin: 0,
        tickLength: 5,
        tickThickness: 1,
        lineThickness: 1,
        valueFormatString: "D MMM YY",
        labelAutoFit: false,
        labelWrap: false,
        labelFontSize: 14,
        interval: 1,
        intervalType: "month",
    };
    options.axisY = {
        margin: -10,
        valueFormatString: " ",
        tickLength: 0,
        minimum: 0,
        lineThickness: 0,
        gridThickness: 1,
        labelMaxWidth: 0,
        gridColor: '#f7f7f7'
    };
    if (threshold != null) {
        options.axisY.stripLines = [
            {
                lineDashType: 'dot',
                color: '#e57373',
                value: threshold,
                showOnTop: true,
                thickness: 2
            }
        ];
    };
    options.data = [
        {
            type: type,
            fillOpacity: .5,
            markerSize: 0,
            color: color,
            dataPoints: data,
            xValueFormatString: "DD MMM YY"
        }
    ];

    return options;
}

function getRiskLineChartOptions(data, commissionDate, threshold, height, width) {

    var options = {};

    if (height != null)
        options.height = height;

    if (width != null)
        options.width = width;

    options.axisX = {};
    options.axisY = {};

    options.axisY.minimum = 0;

    if (threshold != null) {
        options.axisY.stripLines = [
            {
                lineDashType: 'dot',
                color: '#e57373',
                value: threshold,
                showOnTop: true,
                thickness: 2
            }
        ];
    };

    if (!isNaN(commissionDate) && commissionDate != null) {
        options.axisX.stripLines = [
            {
                lineDashType: 'dot',
                label: 'Commission Date',
                color: '#aaa',
                value: commissionDate,
                showOnTop: true,
                thickness: 2
            }
        ]
    }

    options.data = [
        {
            type: 'line',
            showInLegend: true,
            name: 'Risk',
            markerSize: 0,
            color: '#42A5F5',
            dataPoints: data,
            xValueFormatString: "DD MMM YY"
        }
    ];

    return options;
}

function pluckAllWorkItems(data) {

    var output = [];

    for(var d in data) {
        if (data[d].classes == 'gantt-task-work') {
            output.push(data[d]);
        }
    }

    return output;
}

function getCumulativePlot(trace) {
    var cumulative = 0;
    var yearTmp;

    return _.map(trace, function (point, n) {
        // Reset if it's a new year
        if (yearTmp != new Date(point.x).getFullYear()) {
            yearTmp = new Date(point.x).getFullYear();
            
            cumulative = point.y;
        } else {
            cumulative += point.y;
        }

        return {
            x: point.x,
            y: cumulative
        };
    });
}

function getGanttData(data) {

    var output = [];

    // Collections
    for (c in data) {

        var collection = data[c];
        var collectionChildren = [];

        // Milestones
        var milestoneList = [];

        for (m in collection.Milestones) {
            var milestone = collection.Milestones[m];
            var uuid = guid();

            milestoneList.push({
                id: uuid,
                name: milestone.Name,
                data: milestone.Description,
                from: milestone.TargetDate,
                to: milestone.TargetDate,
                color: '#FFB300'
            });
        }

        if (milestoneList.length > 0) {
            var uuid = guid();
            collectionChildren.push(uuid);

            var milestones = {
                id: uuid,
                name: 'Milestones',
                classes: 'gantt-row-milestone',
                tasks: milestoneList
            }
            output.push(milestones);
        }

        // Outages
        var outageList = [];

        for (o in collection.Outages) {

            var outage = collection.Outages[o];
            var outageWorkList = [];
            
            // Collections / Outages / Work
            for (ow in outage.Works) {
                var work = outage.Works[ow];
                var uuid = guid();

                var works = {
                    id: uuid,
                    name: work.Name,
                    classes: 'gantt-task-work',
                    data: {
                        Planned: work.Planned,
                        Name: work.Name,
                        Description: work.Description,
                        WorkOrder: work.WorkOrder,
                        LeadAsset: work.LeadAsset,
                        TargetDate: work.TargetDate,
                        CommissionDate: work.CommissionDate,
                        Duration: work.Duration,
                        Cost: work.Cost,
                        Risk: work.FigurativeRisk,
                        EngineerManDaysRequired: work.EngineerManDaysRequired,
                        SpecialistSkillsRequired: work.SpecialistSkillsRequired,
                        PerformanceStatutoryObligation: work.PerformanceStatutoryObligation,
                        PerformanceNOM: work.PerformanceNOM,
                        PerformanceCustomerConnection: work.PerformanceCustomerConnection,
                        Region: work.Region.Name,
                        Team: work.Team.Name,
                        WorkType: work.WorkType.Name,
                        Zone: work.Zone.Shortcode,
                    },
                    tasks: [
                        {
                            name: work.Name + ' (' + work.WorkType.Name + ')',
                            from: work.TargetDate,
                            to: moment(work.TargetDate).add(work.Duration, 'days'),
                            color: getTaskColor(work.WorkType.Name, work.Planned),
                            content: "",
                        }
                    ]
                }
                outageWorkList.push(uuid);
                output.push(works);
            }
            
            if (outageWorkList.length > 0) {
                var uuid = guid();

                outageList.push(uuid);

                output.push({
                    id: uuid,
                    name: outage.Name,
                    classes: 'gantt-task-outage',
                    data: outage.Description,
                    children: outageWorkList
                });
            }
        }

        if (outageList.length > 0) {
            var uuid = guid();
            collectionChildren.push(uuid);

            output.push({
                id: uuid,
                name: 'Circuits',
                children: outageList
            });
        }

        if (collectionChildren.length > 0) {
            output.push({
                name: collection.Name,
                data: collection.Description,
                children: collectionChildren
            });
        }
    }

    return output;
}

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}