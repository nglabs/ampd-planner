﻿(function () {
    var app = angular.module('workPlannerApp', ['ngAnimate', 'ngStorage', 'ui.router', 'angularMoment', 'gantt', 'gantt.table', 'gantt.movable', 'gantt.groups', 'gantt.tooltips', 'gantt.tree']);

    app.config(function ($stateProvider, $urlRouterProvider) {
        var viewBaseDir = '/app/views/';

        $stateProvider
            .state('home', {
                url: '/home',
                controller: 'HomeController',
                templateUrl: viewBaseDir + 'home/home.html',
            })
            .state('setup', {
                url: '/setup',
                controller: 'SetupController',
                templateUrl: viewBaseDir + 'setup/form.html',
            })
            .state('setup.start', {
                url: '/start',
                templateUrl: viewBaseDir + 'setup/form-start.html',
            })
            .state('setup.timescale', {
                url: '/timescale',
                templateUrl: viewBaseDir + 'setup/form-timescale.html',
            })
            .state('setup.resources', {
                url: '/resources',
                templateUrl: viewBaseDir + 'setup/form-resources.html',
            })
            .state('setup.targets', {
                url: '/targets',
                templateUrl: viewBaseDir + 'setup/form-targets.html',
            })
            .state('plan', {
                url: '/plan',
                controller: 'PlanController',
                templateUrl: viewBaseDir + 'plan/plan.html'
            });

        $urlRouterProvider.otherwise('/home');
    });

    app.service('Settings', function () {
        return {};
    });

    app.run(function ($localStorage, $location) {
    });

}());