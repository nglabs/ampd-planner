﻿(function () {

    var MainController = function ($scope, $state, $location, $localStorage, Settings) {
        Settings = {};

        $scope.newPlan = function () {
            var r = false;

            r = confirm("You will lose all of your progress. Are you sure you would like to continue?");

            if (r) {
                delete $localStorage.ganttData;
                delete $localStorage.settings;
                $location.path('/');
            }
                
        }
    };

    angular.module('workPlannerApp').controller('MainController', MainController);

}());