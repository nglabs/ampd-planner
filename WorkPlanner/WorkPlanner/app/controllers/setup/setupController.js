﻿(function () {

    var SetupController = function ($scope, $http, Settings, $localStorage) {

        var gantt = $localStorage.ganttData;
        var settings = $localStorage.settings;

        if (gantt != null && settings != null) {
            $scope.previousSession = true;
        } else {
            gantt = null;
            settings = null;
        }

        $scope.isCollecting = true;
        $scope.formData = Settings;
        // 1st October 2018
        $scope.formData.start = moment(new Date(2018,9,1)).format('DD/MM/YYYY');
        // 1st September 2021
        $scope.formData.end = moment(new Date(2021, 8, 1)).format('DD/MM/YYYY');
        $scope.formData.budget = 32;
        $scope.formData.engineers = 8;
        $scope.formData.riskLimit = 400;

        $scope.rowClicked = function (obj) {
            obj.Planned = !obj.Planned;
        };

        $http.get("http://ampdworkplannerapi.azurewebsites.net/api/Collections")
        //$http.get("http://localhost:8000/api/Collections")
        .then(function (response) {
            $scope.isCollecting = false;
            $scope.formData.collections = response.data;
        });
    };

    angular.module('workPlannerApp').controller('SetupController', SetupController);

}());