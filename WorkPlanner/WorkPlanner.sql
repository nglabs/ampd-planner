﻿CREATE TABLE dbo.CollectionTypes
(
CollectionTypeID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(50)
)

CREATE TABLE dbo.Collections
(
CollectionID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(50),
CollectionTypeID int FOREIGN KEY REFERENCES CollectionTypes(CollectionTypeID)
)

CREATE TABLE dbo.Milestones
(
MilestoneID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(25) NOT NULL,
Description varchar(max),
TargetDate date,
CollectionID int FOREIGN KEY REFERENCES Collections(CollectionID)
)

CREATE TABLE dbo.Outages
(
OutageID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(25) NOT NULL,
Description varchar(max),
TargetDate date,
Duration int,
CollectionID int FOREIGN KEY REFERENCES Collections(CollectionID)
)

CREATE TABLE dbo.WorkTypes
(
WorkTypeID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(50) NOT NULL
)

CREATE TABLE dbo.Zones
(
ZoneID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(25) NOT NULL,
Shortcode varchar(25)
)

CREATE TABLE dbo.Teams
(
TeamID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(25) NOT NULL
)

CREATE TABLE dbo.Regions
(
RegionID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(25) NOT NULL
)

CREATE TABLE dbo.Work
(
WorkID int IDENTITY(1,1) PRIMARY KEY,
Name varchar(25) NOT NULL,
Description varchar(max),
WorkOrder varchar(25),
LeadAsset varchar(25),
TargetDate date,
Duration int,
Cost decimal(19,4),
EngineerManDaysRequired int,
SpecialistSkillsRequired int,
PerformanceStatutoryObligation tinyint,
PerformanceNOM tinyint,
PerformanceCustomerConnection tinyint,
RegionID int FOREIGN KEY REFERENCES Regions(RegionID),
ZoneID int FOREIGN KEY REFERENCES Zones(ZoneID),
TeamID int FOREIGN KEY REFERENCES Teams(TeamID),
WorkTypeID int FOREIGN KEY REFERENCES WorkTypes(WorkTypeID),
OutageID int FOREIGN KEY REFERENCES Outages(OutageID)
)
